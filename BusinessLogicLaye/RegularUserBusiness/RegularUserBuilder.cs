﻿using System;
using BusinessLogicLayer.Utils;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.RegularUserBusiness
{
    public class RegularUserBuilder : IRegularUserBuilder
    {
        private readonly RegularUser regularUser;

        public RegularUserBuilder()
        {
            regularUser = new RegularUser {DateCreated = DateTime.Now, Id = Guid.NewGuid().ToString()};
        }

        public IRegularUserBuilder AddName(string firstName, string lastName)
        {
            regularUser.FirstName = firstName;
            regularUser.LastName = lastName;

            return this;
        }

        public IRegularUserBuilder AddUsername(string username)
        {
            regularUser.UserName = username;

            return this;
        }

        public IRegularUserBuilder AddEmail(string email)
        {
            regularUser.Email = email;

            return this;
        }

        public IRegularUserBuilder AddPassword(string password)
        {
            regularUser.PasswordHash = password;

            return this;
        }

        public IRegularUserBuilder AddBirthDate(DateTime date)
        {
            regularUser.DateOfBirth = date;
            regularUser.Age = regularUser.DateOfBirth.CalculateAge();

            return this;
        }

        public IRegularUserBuilder AddAddress(string address)
        {
            regularUser.Address = address;

            return this;
        }

        public IRegularUserBuilder AddCity(string city)
        {
            regularUser.City = city;

            return this;
        }

        public RegularUser GetRegularUser()
        {
            return regularUser;
        }
    }
}
