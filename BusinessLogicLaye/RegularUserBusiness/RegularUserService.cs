﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Accounts;
using BusinessLogicLayer.Helpers;
using BusinessLogicLayer.Utils;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogicLayer.RegularUserBusiness
{
    public class RegularUserService : IRegularUserService
    {
        public RegularUser GetRegularUser(string id)
        {
            List<RegularUser> regularUsers = RepositoryLocator.UserRepository.GetDbSet()
                .OfType<RegularUser>()
                .Include(r => r.Accounts)
                .Include(r => r.Bills)
                .ThenInclude(b => b.Company)
                .ToList();

            RegularUser user = regularUsers.Find(u => u.Id.Equals(id));

            return user;
        }

        public SavingAccount GetSavingAccount(int id)
        {
            return RepositoryLocator.AccountRepository.GetDbSet()
                .OfType<SavingAccount>().ToList()
                .FirstOrDefault(a => a.Id == id);
        }

        public SpendingAccount GetSpendingAccount(int id)
        {
            return RepositoryLocator.AccountRepository.GetDbSet()
                .OfType<SpendingAccount>().ToList()
                .FirstOrDefault(a => a.Id == id);
        }

        public async void CreateAccount(RegularUser user, string type, string currency)
        {
            AccountFactory accountFactory = new AccountFactory();

            if (CanCreateAccount(user, type, currency))
            {
                var account = accountFactory.CreateAccountForUser(user, type, currency);

                if (account == null)
                {
                    throw new ValidationException("Could not create account!");
                }
            }
            else
            {
                throw new ValidationException("Cannot create any more accounts of this type!");
            }
        }

        private bool CanCreateAccount(RegularUser user, string type, string currency)
        {
            IEnumerable<Account> accounts = GetAccounts(user).Result;

            int accountCounter = accounts.Count(a => a.Type.Equals(type));

            return accountCounter < 2;
        }

        public async Task<IEnumerable<Account>> GetAccounts(RegularUser user)
        {
            IEnumerable<Account> accounts = await RepositoryLocator.AccountRepository.GetAll();
            var enumerable = accounts.ToList();
            List<Account> accountList = enumerable
                .Where(a => a.User.Id.Equals(user.Id))
                .ToList();

            return accountList;
        }

        public async Task DepositMoneyInSpendingAccount(Account account, double sum)
        {
            SpendingAccount foundAccount = GetSpendingAccount(account.Id);

            if (foundAccount != null)
            {
                if (sum > 0)
                {
                    foundAccount.Sum += sum;
                    foundAccount.DateModified = DateTime.Now;
                
                    RepositoryLocator.AccountRepository.Update(foundAccount);

                    Transaction transaction = EntityFactory.CreateDefaultTransaction(account.User, sum, account, null, "deposit");
                    LogTransaction(transaction);
                }
                else
                {
                    throw new ValidationException("The amount must pe positive!");
                }
                
            }
            else
            {
                throw new ArgumentNullException($"No such account was found!");
            }
        }

        public async Task DepositMoneyInSavingAccount(Account account, double sum, int months)
        {

            SavingAccount foundAccount = GetSavingAccount(account.Id);

            if (foundAccount != null)
            {
                if (sum > 0 && months > 0 )
                {
                    foundAccount.Sum += sum + foundAccount.Commission * months * sum;
                    foundAccount.DateModified = DateTime.Now;
                
                    RepositoryLocator.AccountRepository.Update(foundAccount);

                    Transaction transaction = EntityFactory.CreateDefaultTransaction(account.User, sum, account, null, "deposit");
                    LogTransaction(transaction);
                }
                 else
                {
                    throw new ValidationException("The amount and the number of months must be positive!");
                }
            }
            else
            {
                throw new ArgumentNullException($"No such account was found!");
            }
        }

        public async Task<IEnumerable<string>> GetAllCurrencies()
        {
            IEnumerable<string> currencies = RepositoryLocator.CurrencyRepository
                .GetAll()
                .Result
                .Select(c => c.Name)
                .Distinct()
                .AsEnumerable();

            return currencies;
        }

        public async Task<IEnumerable<Bill>> GetBills(RegularUser user)
        {
            return RepositoryLocator.BillRepository.GetAll().Result;
        }

        public void PayBill(RegularUser user, Bill bill, Account account)
        {
            Bill foundBill = GetBill(bill.Id.ToString());

            Account foundAccount = GetAccount(account.Id);

            if (foundBill != null && foundAccount != null)
            {
                if (account is SpendingAccount spendingAccount)
                {
                    PayBillFromSpendingAccount(bill, spendingAccount);
                }

                if (account is SavingAccount savingAccount)
                {
                    PayBillFromSavingAccount(bill, savingAccount);
                }
            }
        }

        private void PayBillFromSavingAccount(Bill bill, SavingAccount savingAccount)
        {                
            double exhangeMoneyInAccount = CurrencyConverter
                .GetExchangeRate(savingAccount.Currency, bill.Company.Currency, savingAccount.Sum);

            if (bill.Amount <= exhangeMoneyInAccount)
            {
                double amountToPay = CurrencyConverter
                    .GetExchangeRate(bill.Company.Currency, savingAccount.Currency, bill.Amount);

                if (bill.Company.Currency.Equals(savingAccount.Currency))
                {
                    savingAccount.Sum -= amountToPay;
                }
                else
                {
                    savingAccount.Sum -=  Math.Round(amountToPay * 1.03);  // 3% commission
                }
                
                savingAccount.DateModified = DateTime.Now;

                RepositoryLocator.AccountRepository.Update(savingAccount);
                bill.IsActive = false;
                bill.DatePaid = DateTime.Now;
                RepositoryLocator.BillRepository.Update(bill);

                Transaction transaction = EntityFactory.CreateDefaultTransaction(savingAccount.User, bill.Amount, savingAccount, bill, "payment");
                LogTransaction(transaction);
            }
            else
            {
                throw new ValidationException("Not enough money to pay the bill.");
            }
        }

        private void PayBillFromSpendingAccount(Bill bill, SpendingAccount spendingAccount)
        {
            double exhangeMoneyInAccount = CurrencyConverter
                .GetExchangeRate(spendingAccount.Currency, bill.Company.Currency, spendingAccount.Sum);

            if (bill.Amount - 2000 <= exhangeMoneyInAccount)
            {
                double amountToPay = CurrencyConverter
                    .GetExchangeRate(bill.Company.Currency, spendingAccount.Currency, bill.Amount);
                if (bill.Company.Currency.Equals(spendingAccount.Currency))
                {
                    spendingAccount.Sum -= amountToPay + Math.Round(spendingAccount.InterestRate * amountToPay, 1);
                }
                else
                {
                    spendingAccount.Sum -= Math.Round((amountToPay +  spendingAccount.InterestRate * amountToPay) * 1.03);
                }
                spendingAccount.DateModified = DateTime.Now;
                RepositoryLocator.AccountRepository.Update(spendingAccount);

                bill.IsActive = false;
                bill.DatePaid = DateTime.Now;
                RepositoryLocator.BillRepository.Update(bill);

                Transaction transaction = EntityFactory.CreateDefaultTransaction(spendingAccount.User, bill.Amount, spendingAccount, bill, "payment");
                LogTransaction(transaction);
            }
            else
            {
                throw new ValidationException("Not enough money to pay the bill.");
            }
        }

        public Account GetAccount(int id)
        {
            return RepositoryLocator.AccountRepository.GetAll().Result.FirstOrDefault(x => x.Id == id);
        }

        public Bill GetBill(string id)
        {
            return RepositoryLocator.BillRepository.GetAll().Result
                .FirstOrDefault(x => x.Id.ToString().Equals(id));
        }

        private void LogTransaction(Transaction transaction)
        {
            RepositoryLocator.TransactionRepository.Insert(transaction);
        }

        public List<Transaction> GetTransactions(RegularUser user)
        {
            List<Transaction> transactions = RepositoryLocator.TransactionRepository.GetDbSet()
                .Include(t => t.User)
                .Include(t => t.Account)
                .Include(t => t.Bill)
                .ThenInclude(b => b.Company).ToList();

            return transactions;
        }

        public List<Transaction> GetTransactions(RegularUser user, DateTime from, DateTime to)
        {
            List<Transaction> transactions = GetTransactions(user).ToList();

            from = from.Date.AddHours(0).AddMinutes(0).AddSeconds(0);
            to = to.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            if (from < to)
            {
                List<Transaction> transactionsPerPeriod = transactions
                    .Where(t => t.Date >= from && t.Date <= to).ToList();

                return transactionsPerPeriod;
            }
            else
            {
                throw new ValidationException("The display period is not correct.");
            }
        }
    }
}
