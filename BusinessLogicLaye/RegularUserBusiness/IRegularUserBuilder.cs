﻿using System;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.RegularUserBusiness
{
    public interface IRegularUserBuilder
    {
        IRegularUserBuilder AddName(string firstName, string lastName);
        IRegularUserBuilder AddUsername(string username);
        IRegularUserBuilder AddEmail(string email);
        IRegularUserBuilder AddPassword(string password);
        IRegularUserBuilder AddBirthDate(DateTime date);
        IRegularUserBuilder AddAddress(string address);
        IRegularUserBuilder AddCity(string city);

        RegularUser GetRegularUser();
    }
}
