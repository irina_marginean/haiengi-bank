﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.RegularUserBusiness
{
    public interface IRegularUserService
    {
        RegularUser GetRegularUser(string id);
        Account GetAccount(int id);
        SavingAccount GetSavingAccount(int id);
        SpendingAccount GetSpendingAccount(int id);
        void CreateAccount(RegularUser user, string type, string currency);
        Task<IEnumerable<Account>> GetAccounts(RegularUser user);
        Task DepositMoneyInSpendingAccount(Account account, double sum);
        Task DepositMoneyInSavingAccount(Account account, double sum, int months);
        Task<IEnumerable<string>> GetAllCurrencies();
        Task<IEnumerable<Bill>> GetBills(RegularUser user);
        Bill GetBill(string id);
        void PayBill(RegularUser user, Bill bill, Account account);
        List<Transaction> GetTransactions(RegularUser user);
        List<Transaction> GetTransactions(RegularUser user, DateTime from, DateTime to);
    }
}
