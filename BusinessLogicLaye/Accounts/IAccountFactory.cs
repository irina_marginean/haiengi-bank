﻿using System.Threading.Tasks;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Accounts
{
    public interface IAccountFactory
    {
        Task<Account> CreateAccount(RegularUser regularUser, string currency);
    }
}
