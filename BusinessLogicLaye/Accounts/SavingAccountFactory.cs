﻿using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Utils;
using DataAccessLayer;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Accounts
{
    public class SavingAccountFactory : IAccountFactory
    { 
        public async Task<Account> CreateAccount(RegularUser regularUser, string currency)
        {
            var currencies = await RepositoryLocator.CurrencyRepository.GetAll();

            Currency foundCurrency = currencies
                .FirstOrDefault(x => x.Name.Equals(currency));

            if (foundCurrency != null)
            {
                double commission = foundCurrency.Commission;

                SavingAccount savingAccount = EntityFactory.CreateDefaultSavingAccount(regularUser, currency, commission);

                RepositoryLocator.AccountRepository.Insert(savingAccount);

                return savingAccount;
            }

            return null;
        }
    }
}
