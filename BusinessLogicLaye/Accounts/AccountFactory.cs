﻿using DataAccessLayer.Models;

namespace BusinessLogicLayer.Accounts
{
    public sealed class AccountFactory
    {
        private IAccountFactory accountFactory;

        public Account CreateAccountForUser(RegularUser regularUser, string type, string currency)
        {
            switch (type)
            {
                case "spending":
                    accountFactory =  new SpendingAccountFactory();
                    break;
                case "saving":
                    accountFactory =  new SavingAccountFactory();
                    break;
                default:
                    return null;
            }

            return accountFactory.CreateAccount(regularUser, currency).Result;
        }
    }
}
