﻿using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Utils;
using DataAccessLayer;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Accounts
{
    public class SpendingAccountFactory : IAccountFactory
    {
        public async Task<Account> CreateAccount(RegularUser regularUser, string currency)
        {
            var currencies = await RepositoryLocator.CurrencyRepository.GetAll();
            Currency foundCurrency = currencies
                .FirstOrDefault(x => x.Name.Equals(currency));

            if (foundCurrency != null)
            {
                double interest = foundCurrency.Interest;

                SpendingAccount spendingAccount = EntityFactory.CreateDefaultSpendingAccount(regularUser, currency, interest);

                RepositoryLocator.AccountRepository.Insert(spendingAccount);

                return spendingAccount;
            }

            return null;
        }
    }
}
