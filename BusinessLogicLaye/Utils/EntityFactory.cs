﻿using System;
using DataAccessLayer.Models;

namespace BusinessLogicLayer.Utils
{
    public static class EntityFactory
    {
        public static SpendingAccount CreateDefaultSpendingAccount(RegularUser user, string currency, double percentage)
        {
            SpendingAccount spendingAccount = new SpendingAccount
            {
                User = user,
                Currency = currency,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                InterestRate = percentage,
                Sum = 0,
                Type = "spending"
            };

            spendingAccount.IBAN = IBANGenerator.GenerateIBAN(spendingAccount);

            return spendingAccount;
        }

        public static SavingAccount CreateDefaultSavingAccount(RegularUser user, string currency, double percentage)
        {
            SavingAccount savingAccount = new SavingAccount
            {
                User = user,
                Currency = currency,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                Commission = percentage,
                Sum = 0,
                Type = "saving"
            };

            savingAccount.IBAN = IBANGenerator.GenerateIBAN(savingAccount);

            return savingAccount;
        }

        public static Bill CreateDefaultBill(double amount, Company company, RegularUser user)
        {
            Bill bill = new Bill
            {
                Id = Guid.NewGuid(),
                Amount = amount,
                Company = company,
                DateCreated = DateTime.Now,
                User = user,
                IsActive = true
            };

            return bill;
        }

        public static Transaction CreateDefaultTransaction(RegularUser user, double amount, Account account, Bill bill, string type)
        {
            Transaction transaction = new Transaction
            {
                Id = Guid.NewGuid(),
                User = user,
                Account = account,
                Amount = amount,
                Bill = bill,
                Date = DateTime.Now,
                Type = type
            };

            return transaction;
        }
    }
}
