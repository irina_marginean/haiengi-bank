﻿using MailKit.Security;
using MimeKit;

namespace BusinessLogicLayer.Utils
{
    public static class EmailNotifier
    {
        public static void SendEmail(MimeMessage messageToSend)
        {
            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {

                client.Connect("smtp.gmail.com", 587, false);

                client.Authenticate("office.haiengibank@gmail.com", "123banane");

                client.Send(messageToSend);

                client.Disconnect(true);
            }
        }
    }
}
