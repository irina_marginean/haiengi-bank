﻿using DataAccessLayer.Models;

namespace BusinessLogicLayer.Utils
{
    public static class IBANGenerator
    {
        public static string GenerateIBAN(Account account)
        {
            char type = GetAccountType(account);
            string country = GetCountry(account);
            string numbers = NumberGenerator.GetRandomNumber(5);
            string firstName = account.User.FirstName.Substring(0, 2).ToUpper();
            string lastName = account.User.LastName.Substring(0, 2).ToUpper();
            int date = account.DateCreated.Minute + account.DateCreated.Hour;
            string currency = account.Currency;
            string numbers2 = NumberGenerator.GetRandomNumber(6);
            string city = account.User.City;

            string IBAN = $"HNG{type}{country}{numbers}{firstName}{lastName}{date}{currency}{numbers2}{city}";

            return IBAN;
        }

        private static char GetAccountType(Account account)
        {
            if (account.Type == "saving")
            {
                return '*';
            }

            if (account.Type == "spending")
            {
                return '#';
            }

            return '?';
        }

        private static string GetCountry(Account account)
        {
            string[] countries = new[] {"RO", "EN", "GB", "HU", "IT"};

            int index = NumberGenerator.GetRandomNumberInRange(0, countries.Length - 1);

            return countries[index];
        }
    }
}
