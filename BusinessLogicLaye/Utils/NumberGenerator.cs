﻿using System;

namespace BusinessLogicLayer.Utils
{
    public static class NumberGenerator
    {
        public static int GetRandomNumberInRange(int minNumber, int maxNumber)
        {
            return new Random().Next(minNumber, maxNumber);
        }

        public static double GetRandomDoubleInRange(int minNumber, int maxNumber)
        {
            return  Math.Round(new Random().NextDouble() * (maxNumber - minNumber) + minNumber, 1);
        }

        public static string GetRandomNumber(int numberOfDigits)
        {
            string number = "";

            for (int i = 0; i < numberOfDigits; i++)
            {
                number += GetRandomNumberInRange(0, 9).ToString();
            }

            return number;
        }
    }
}
