﻿using Microsoft.AspNetCore.Identity;

namespace BusinessLogicLayer.Helpers
{
    public class BCryptPasswordHasher : IPasswordHasher<IdentityUser>
    {
        public string HashPassword(IdentityUser user, string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(SaltPassword(user, password), 10);
        }

        private string SaltPassword(IdentityUser user, string password)
        {
            return "haiengi_yourbank";
        }

        public PasswordVerificationResult VerifyHashedPassword(IdentityUser user, string hashedPassword, string providedPassword)
        {
            if (BCrypt.Net.BCrypt.Verify(SaltPassword(user, providedPassword), hashedPassword))
            {
                return PasswordVerificationResult.Success;
            }

            return PasswordVerificationResult.Failed;
        }
    }
}
