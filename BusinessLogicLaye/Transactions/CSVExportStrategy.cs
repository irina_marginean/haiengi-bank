﻿using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Transactions
{
    public class CSVExportStrategy : ITransactionExportStrategy
    {
        public byte[] GetExportBytes(List<Transaction> transactions)
        {
            StringBuilder stringBuilder = new StringBuilder();

            List<TransactionDTO> transactionDTOs = new List<TransactionDTO>();

            foreach (var transaction in transactions)
            {
                transactionDTOs.Add(new TransactionDTO(transaction));
            }

            foreach (var transactionDTO in transactionDTOs)
            {
                stringBuilder.AppendLine(
                    $"{transactionDTO.Username}," +
                    $"{transactionDTO.Email}," +
                    $"{transactionDTO.FirstName}," +
                    $"{transactionDTO.LastName}" +
                    $"{transactionDTO.Amount}," +
                    $"{transactionDTO.AccountIBAN}," +
                    $"{transactionDTO.AccountCurrency}," +
                    $"{transactionDTO.Date}," +
                    $"{transactionDTO.BillCompany}," +
                    $"{transactionDTO.BillCurrency}," +
                    $"{transactionDTO.DateCreatedBill}," +
                    $"{transactionDTO.DatePaidBill}");
            }

           return new UTF8Encoding().GetBytes(stringBuilder.ToString());
        }

        public string GetFileName()
        {
            return "transactions.csv";
        }

        public string GetFileType()
        {
            return "text/csv";
        }
    }
}
