﻿using DataAccessLayer.Models;
using System.Collections.Generic;

namespace BusinessLogicLayer.Transactions
{
    public class ExportStrategy
    {
        private readonly ITransactionExportStrategy transactionExportStrategy;

        public ExportStrategy(ITransactionExportStrategy transactionExportStrategy)
        {
            this.transactionExportStrategy = transactionExportStrategy;
        }

        public byte[] GetExportBytes(List<Transaction> transactions)
        {
            return transactionExportStrategy.GetExportBytes(transactions);
        }

        public string GetFileName()
        {
            return transactionExportStrategy.GetFileName();
        }

        public string GetFileType()
        {
            return transactionExportStrategy.GetFileType();
        }
    }
}
