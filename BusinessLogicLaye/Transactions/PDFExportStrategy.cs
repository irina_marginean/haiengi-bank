﻿using DataAccessLayer.Models;
using RandomSolutions;
using System.Collections.Generic;

namespace BusinessLogicLayer.Transactions
{
    public class PDFExportStrategy : ITransactionExportStrategy
    {
        public byte[] GetExportBytes(List<Transaction> transactions)
        {
            List<TransactionDTO> transactionDTOs = new List<TransactionDTO>();

            foreach (var transaction in transactions)
            {
                transactionDTOs.Add(new TransactionDTO(transaction));
            }

            byte[] pdf = transactionDTOs.ToPdf();

            return pdf;
        }

        public string GetFileName()
        {
            return "transactions.pdf";
        }

        public string GetFileType()
        {
            return "text/pdf";
        }
    }
}
