﻿using DataAccessLayer.Models;
using System;

namespace BusinessLogicLayer.Transactions
{
    internal class TransactionDTO
    {
        public string Username { get; private set; } 
        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public double Amount { get; private set; }
        public string AccountIBAN { get; private set; }
        public string AccountCurrency { get; private set; }
        public DateTime Date { get; private set; }
        public string? BillCompany { get; private set;}
        public string? BillCurrency { get; private set;}
        public DateTime? DateCreatedBill { get; private set; }
        public DateTime? DatePaidBill { get; private set; }

        public TransactionDTO(Transaction transaction)
        {
            Username = transaction.User.UserName;
            Email = transaction.User.Email;
            FirstName = transaction.User.FirstName;
            LastName = transaction.User.LastName;
            Amount = transaction.Amount;
            AccountIBAN = transaction.Account.IBAN;
            AccountCurrency = transaction.Account.Currency;
            Date = transaction.Date;
            BillCompany = transaction.Bill?.Company.Name;
            BillCurrency = transaction.Bill?.Company.Currency;
            DateCreatedBill = transaction.Bill?.DateCreated;
            DatePaidBill = transaction.Bill?.DatePaid;
        }
    }
}
