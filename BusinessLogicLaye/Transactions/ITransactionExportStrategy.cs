﻿using DataAccessLayer.Models;
using System.Collections.Generic;

namespace BusinessLogicLayer.Transactions
{
    public interface ITransactionExportStrategy
    {
        byte[] GetExportBytes(List<Transaction> transactions);
        string GetFileName();
        string GetFileType();
    }
}
