﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;

namespace BusinessLogicLayer.AdminBusiness
{
    public interface IAdminService
    {
        Task<IEnumerable<IdentityUser>> GetAllUsers();
        Task<IEnumerable<RegularUser>> GetRegularUsers();
        void NotifyUserOfNegativeAccountSum(RegularUser regularUser, Account account);
        Task<IEnumerable<Company>> GetAllCompanies();
        void AddCompany(Company company);
        void GenerateRandomBills(RegularUser user);
        Task<IEnumerable<string>> GetAllCurrencies();
    }
}
