﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Utils;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Text;


namespace BusinessLogicLayer.AdminBusiness
{
    public class AdminService : IAdminService
    {
        public async Task<IEnumerable<IdentityUser>> GetAllUsers()
        { 
            return await RepositoryLocator.UserRepository.GetAll();
        }

        public async Task<IEnumerable<RegularUser>> GetRegularUsers()
        {
            return RepositoryLocator.UserRepository.GetDbSet()
                .OfType<RegularUser>()
                .Include(r => r.Accounts)
                .Include(r => r.Bills)
                .ThenInclude(b => b.Company);
        }

        public void NotifyUserOfNegativeAccountSum(RegularUser regularUser, Account account)
        {
            if (account.Sum < 0)
            {
                var messageToSend = new MimeMessage
                {
                    Sender = new MailboxAddress("Admin of HaiEnGi", "admin@haiengi.com"),
                    Subject = "Negative Account Amount Notification"
                };
                messageToSend.To.Add(new MailboxAddress(regularUser.Email));

                messageToSend.Body = new TextPart(TextFormat.Plain)
                {
                    Text = $"Hello there,\n" +
                           $"It seems like the amount of your account having the IBAN {account.IBAN} has dropped below zero!" +
                           $"We recommend you make a deposit as soon as possible.\n" +
                           $"Kind regards,\n" +
                           $"The HaiEnGi Bank"
                };

                EmailNotifier.SendEmail(messageToSend);
            }
            else
            {
                throw new ValidationException("The account does not have negative amount");
            }     
        }

        public async Task<IEnumerable<Company>> GetAllCompanies()
        {
            return await RepositoryLocator.CompanyRepository.GetAll();
        }

        public async void AddCompany(Company company)
        {
            Company foundCompany = GetAllCompanies().Result.FirstOrDefault(c => c.Name.Equals(company.Name));

            if (foundCompany == null)
            {
                RepositoryLocator.CompanyRepository.Insert(company);
            }
            else
            {
                throw new ValidationException("This company already exists!");
            }
        }

        public async void GenerateRandomBills(RegularUser user)
        {
            IEnumerable<Company> companies = await GetAllCompanies();
            var enumerable = companies.ToList();

            int numberOfBills = NumberGenerator.GetRandomNumberInRange(1, 5);

            for (int i = 0; i < numberOfBills; i++)
            {
                double amount = NumberGenerator.GetRandomDoubleInRange(1, 500);
                int companyIndex = NumberGenerator.GetRandomNumberInRange(0, enumerable.Count - 1);
                Company company = enumerable.ElementAt(companyIndex);

                Bill bill = EntityFactory.CreateDefaultBill(amount, company, user);

                RepositoryLocator.BillRepository.Insert(bill);
            }
        }

        public async Task<IEnumerable<string>> GetAllCurrencies()
        {
            IEnumerable<string> currencies = RepositoryLocator.CurrencyRepository
                .GetAll()
                .Result
                .Select(c => c.Name)
                .Distinct()
                .AsEnumerable();

            return currencies;
        }
    }
}
