using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.AdminBusiness;
using DataAccessLayer.Models;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Testing
{
    public class Tests
    {
        [TestMethod]
        public void TestDB()
        {
            IAdminService adminService = new AdminService();

            List<User> users = adminService.GetAllUsers().ToList();

            (users.Count > 0);
        }
    }
}