﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public interface IRepository<T> where T : class
    {
        Task<IQueryable<T>> GetQueryable();
        Task<IEnumerable<T>> GetAll();
        DbSet<T> GetDbSet();
        void Insert(T entity);
        void Update(T entityToUpdate);
        void Delete(T entityToDelete);
    }
}
