﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        private readonly DataContext dataContext;
        private readonly DbSet<T> dbSet;

        public BaseRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
            dbSet = dataContext.Set<T>();
        }

        public Task<IQueryable<T>> GetQueryable()
        {
            return Task.FromResult(dbSet.AsQueryable());
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await Task.FromResult(dbSet.AsEnumerable());
        }

        public DbSet<T> GetDbSet()
        {
            return dbSet;
        }

        public void Insert(T entity)
        {
            dbSet.Add(entity);

            dataContext.SaveChanges();
        }

        public void Update(T entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            dataContext.Entry(entityToUpdate).State = EntityState.Modified;

            dataContext.SaveChanges();
        }

        public void Delete(T entityToDelete)
        {
            if (dataContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);

            dataContext.SaveChanges();
        }
    }
}
