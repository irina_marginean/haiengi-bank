﻿using System;
using System.Collections.Generic;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Seeders
{
    internal static class CompanySeeder
    {
        private static List<Company> CompanyList { get; } = GetData();

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (Company company in CompanyList)
            {
                modelBuilder.Entity<Company>().HasData(company);
            }
        }

        private static List<Company> GetData()
        {
            List<Company> companies = new List<Company>
            {
                new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "Orange",
                    Currency = "RON"
                },
                new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "e-on",
                    Currency = "RON"
                },
                new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "Electrica",
                    Currency = "EUR"
                },
                new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "RCS RDS",
                    Currency = "EUR"
                }
            };

            return companies;
        }
    }
}
