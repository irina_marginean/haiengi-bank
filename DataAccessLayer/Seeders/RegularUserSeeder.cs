﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using RegularUser = DataAccessLayer.Models.RegularUser;

namespace DataAccessLayer.Seeders
{
    internal static class RegularUserSeeder
    {
        private static List<RegularUser> UserList = new List<RegularUser>
        {
            new RegularUser
            {
                Id = "id3",
                Email = "irina@gmail.com",
                UserName = "irina",
                FirstName = "Irina",
                LastName = "Marginean",
                PasswordHash = "password",
                City = "CJ",
                Address = "Dorobantilor, 71",
                Age = 21,
                DateCreated = DateTime.Now,
                DateOfBirth = new DateTime(1998, 12, 4)
            },
            new RegularUser
            {
                Id = "id4",
                Email = "gigel@gmail.com",
                UserName = "gigel",
                FirstName = "Gigel",
                LastName = "Costel",
                PasswordHash = "password",
                City = "CJ",
                Address = "Dorobantilor 73",
                Age = 50,
                DateCreated = DateTime.Now,
                DateOfBirth = new DateTime(1970, 4, 4)
            }
        };

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (RegularUser user in UserList)
            {
                modelBuilder.Entity<RegularUser>().HasData(user);
            }
        }
    }
}
