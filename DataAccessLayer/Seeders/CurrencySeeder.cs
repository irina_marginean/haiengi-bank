﻿using System;
using System.Collections.Generic;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Seeders
{
    internal static class CurrencySeeder
    {
        private static List<Currency> CurrencyList { get; } = GetData();

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (Currency currency in CurrencyList)
            {
                modelBuilder.Entity<Currency>().HasData(currency);
            }
        }

        private static List<Currency> GetData()
        {
            List<Currency> currencies = new List<Currency>
            {
                new Currency
                {
                    Id = Guid.NewGuid(),
                    Name = "RON",
                    Interest = 0.2,
                    Commission = 0.3
                },
                new Currency
                {
                    Id = Guid.NewGuid(),
                    Name = "EUR",
                    Interest = 0.3,
                    Commission = 0.6
                },
                new Currency
                {
                    Id = Guid.NewGuid(),
                    Name = "GBP",
                    Interest = 0.1,
                    Commission = 0.3
                },
                new Currency
                {
                    Id = Guid.NewGuid(),
                    Name = "HUF",
                    Interest = 0.1,
                    Commission = 0.2
                },
            };

            return currencies;
        }
    }
}
