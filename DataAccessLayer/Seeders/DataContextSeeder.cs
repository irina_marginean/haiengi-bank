﻿using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Seeders
{
    public static class DataContextSeeder
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            CompanySeeder.Seed(modelBuilder);
            CurrencySeeder.Seed(modelBuilder);
        }
    }
}
