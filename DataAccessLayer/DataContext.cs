﻿using System;
using DataAccessLayer.Models;
using DataAccessLayer.Seeders;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Account = DataAccessLayer.Models.Account;
using Admin = DataAccessLayer.Models.Admin;
using RegularUser = DataAccessLayer.Models.RegularUser;
using SavingAccount = DataAccessLayer.Models.SavingAccount;
using SpendingAccount = DataAccessLayer.Models.SpendingAccount;

namespace DataAccessLayer
{
    public class DataContext : IdentityDbContext<IdentityUser>
    {
        public override DbSet<IdentityUser> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<Transaction> Transactions {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseSqlite(@"DataSource=D:\Personal Projects\SD\software-design-2020-30433-assignment2-irinamarginean\DataAccessLayer\haiengi.sqlite");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
                
            modelBuilder.Entity<RegularUser>();
            modelBuilder.Entity<Admin>();

            modelBuilder.Entity<SpendingAccount>();
            modelBuilder.Entity<SavingAccount>();

            modelBuilder.Entity<RegularUser>()
                .HasMany(ru => ru.Accounts)
                .WithOne(a => a.User);

            modelBuilder.Entity<RegularUser>()
                .HasMany(ru => ru.Bills)
                .WithOne(b => b.User);

            DataContextSeeder.Seed(modelBuilder);
        }
    }
}
