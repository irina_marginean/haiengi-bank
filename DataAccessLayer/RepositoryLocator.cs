﻿using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using Account = DataAccessLayer.Models.Account;

namespace DataAccessLayer
{
    public static class RepositoryLocator
    {
        private static readonly DataContext DataContext = new DataContext();

        private static IRepository<IdentityUser> userRepository;
        private static IRepository<Account> accountRepository;
        private static IRepository<Currency> currencyRepository;
        private static IRepository<Company> companyRepository;
        private static IRepository<Bill> billRepository;
        private static IRepository<Transaction> transactionRepository;

        public static IRepository<IdentityUser> UserRepository =>
            userRepository ??= new BaseRepository<IdentityUser>(DataContext);

        public static IRepository<Account> AccountRepository =>
            accountRepository ??= new BaseRepository<Account>(DataContext);

        public static IRepository<Currency> CurrencyRepository =>
            currencyRepository ??= new BaseRepository<Currency>(DataContext);

        public static IRepository<Company> CompanyRepository =>
            companyRepository ??= new BaseRepository<Company>(DataContext);

        public static IRepository<Bill> BillRepository =>
            billRepository ??= new BaseRepository<Bill>(DataContext);

        public static IRepository<Transaction> TransactionRepository =>
            transactionRepository ??= new BaseRepository<Transaction>(DataContext);
    }
}
