﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Transaction
    {
        [Key]
        public Guid Id {get; set;}
        public string Type {get; set;}
        public double Amount {get; set;}
        public RegularUser User {get; set;}
        public Account Account {get; set;}
        public Bill Bill {get; set;}
        public DateTime Date {get; set;}
    }
}
