﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Currency
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Interest { get; set; }
        public double Commission { get; set; }
    }
}
