﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Bill
    {
        [Key]
        public Guid Id { get; set; }
        public Company Company { get; set; }
        public RegularUser User { get; set; }
        public double Amount { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DatePaid { get; set; }
        public bool IsActive { get; set; }
    }
}
