﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Company
    {
        [Key]
        public Guid Id { get; set; }
        public string Name{ get; set; }
        public string Currency{ get; set; }
    }
}
