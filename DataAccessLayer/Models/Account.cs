﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models
{
    public abstract class Account
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string IBAN{ get; set; }
        public  DateTime DateCreated { get; set; }
        public  DateTime DateModified { get; set; }
        public string Currency { get; set; }
        public string Type { get; set; }
        public double Sum { get; set; }
        public RegularUser User { get; set; }
    }
}
