﻿namespace DataAccessLayer.Models
{
    public class SpendingAccount : Account
    {
        public double InterestRate { get; set; }
    }
}
