﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace DataAccessLayer.Models
{
    public class RegularUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateCreated { get; set; }
        public List<Account> Accounts { get; set; }
        public List<Bill> Bills { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
}
