﻿namespace DataAccessLayer.Models
{
    public class SavingAccount : Account
    {
        public double Commission { get; set; }
    }
}
