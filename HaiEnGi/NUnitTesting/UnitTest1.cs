using BusinessLogicLayer.Transactions;
using Moq;
using NUnit.Framework;

namespace NUnitTesting
{
    public class Tests
    {
        Mock<ITransactionExportStrategy> exportStrategyMock;
        string fileType;
        string fileName;

        [SetUp]
        public void Setup()
        {
            exportStrategyMock = new Mock<ITransactionExportStrategy>();

            fileType = "doc";
            fileName = "transactions.docx";
        }

        [Test]
        public void ExportStrategyTest()
        {
            exportStrategyMock.Setup(x => x.GetFileType())
                .Returns(fileType);
            exportStrategyMock.Setup(x => x.GetFileName())
                .Returns(fileName);

            ExportStrategy exportStrategy = new ExportStrategy(exportStrategyMock.Object);

            Assert.AreEqual(fileType, exportStrategy.GetFileType());
            Assert.AreEqual(fileName, exportStrategy.GetFileName());
        }
    }
}