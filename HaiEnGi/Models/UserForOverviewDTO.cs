﻿namespace HaiEnGi.Models
{
    public class UserForOverviewDTO
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string City { get; set; }
    }
}
