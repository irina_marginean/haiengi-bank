﻿namespace HaiEnGi.Models
{
    public class UserForLoginDTO
    {
        public string Id{ get; set; }
        public string Password { get; set; }
    }
}
