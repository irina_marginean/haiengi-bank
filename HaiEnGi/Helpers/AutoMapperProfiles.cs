﻿using AutoMapper;
using DataAccessLayer.Models;
using HaiEnGi.Models;

namespace HaiEnGi.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForOverviewDTO>();
        }
    }
}
