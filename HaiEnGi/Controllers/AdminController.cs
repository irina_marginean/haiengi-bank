﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogicLayer.AdminBusiness;
using DataAccessLayer;
using DataAccessLayer.Models;
using HaiEnGi.Models;
using Microsoft.AspNetCore.Mvc;

namespace HaiEnGi.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAdminService adminService;
        private readonly IMapper mapper;

        public AdminController(IAdminService adminService, IMapper mapper)
        {
            this.adminService = adminService;
            this.mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ViewAllUsers()
        {
            List<User> users = adminService.GetAllUsers().ToList();

            List<UserForOverviewDTO> userForOverviewDtos = mapper.Map<List<UserForOverviewDTO>>(users);

            return View(userForOverviewDtos);
        }


        public IActionResult Details()
        {
            throw new System.NotImplementedException();
        }
    }
}