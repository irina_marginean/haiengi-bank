﻿using BusinessLogicLayer.Authentication;
using DataAccessLayer.Models;
using HaiEnGi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HaiEnGi.Controllers
{
    public class AuthController : Controller
    {
        private readonly ILogger<AuthController> logger;

        private readonly IAuthService authService;

        public AuthController(IAuthService authService, ILogger<AuthController> logger)
        {
            this.authService = authService;
            this.logger = logger;
        }

        public ViewResult Index()
        {
            return View();
        }

        public IActionResult Login(UserForLoginDTO user)
        {
            User userFromRepo = authService.Login(user.Id, user.Password);

            return View(user);
        }
    }
}
