﻿using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;

namespace PresentationLayer.Helpers
{
    public static class Extensions
    {
        public static string SetUserType(this IdentityUser user)
        {
            if (user is RegularUser)
            {
                return "Regular user";
            }

            if (user is Admin)
            {
                return "Admin";
            }

            return "";
        }

        public static double SetAccountPercentage(this Account account)
        {
            if (account is SavingAccount savingAccount)
            {
                return savingAccount.Commission;
            }

            if (account is SpendingAccount spendingAccount)
            {
                return spendingAccount.InterestRate;
            }

            return 0;
        }

        public static bool SetTransactionExportStatus(this Transaction account)
        {
            return false;
        }
    }
}
