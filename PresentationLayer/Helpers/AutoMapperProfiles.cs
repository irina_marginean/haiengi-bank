﻿using AutoMapper;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using PresentationLayer.Models;

namespace PresentationLayer.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<IdentityUser, UserForOverviewDTO>()
                .ForMember(dest => dest.Type, opt => 
                    opt.MapFrom(src => src.SetUserType()));

            CreateMap<RegularUser, RegularUserForDetailsDTO>();

            CreateMap<SavingAccount, AccountForDetailsDTO>()
                .ForMember(dest => dest.Percentage, 
                    opt => opt.MapFrom(
                        src => src.Commission));

            CreateMap<SpendingAccount, AccountForDetailsDTO>()
                .ForMember(dest => dest.Percentage, 
                    opt => opt.MapFrom(
                        src => src.InterestRate));

            CreateMap<Transaction, TransactionForExportDTO>()
                .ForMember(dest => dest.IsChecked,
                    opt => opt.MapFrom(
                        src => false));
        }
    }
}
