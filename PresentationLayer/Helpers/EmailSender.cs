﻿using BusinessLogicLayer.Utils;
using Microsoft.AspNetCore.Identity.UI.Services;
using MimeKit;
using MimeKit.Text;
using System.Threading.Tasks;

namespace PresentationLayer.Helpers
{
    public class EmailSender : IEmailSender
    {
        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
             var messageToSend = new MimeMessage
                {
                    Sender = new MailboxAddress("Admin of HaiEnGi", "admin@haiengi.com"),
                    Subject = subject
                };
                messageToSend.To.Add(new MailboxAddress(email));

                messageToSend.Body = new TextPart(TextFormat.Html)
                {
                    Text = htmlMessage
                };

                EmailNotifier.SendEmail(messageToSend);
        }
    }
}
