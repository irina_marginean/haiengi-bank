﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(PresentationLayer.Areas.Identity.IdentityHostingStartup))]
namespace PresentationLayer.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}