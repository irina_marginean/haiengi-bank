﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace PresentationLayer
{
    public static class SeedData
    {
        public static async Task InitializeAsync(IServiceProvider services)
        {
            var roleManager = services
                .GetRequiredService<RoleManager<IdentityRole>>();
            await EnsureRolesAsync(roleManager);

            var userManager = services
                .GetRequiredService<UserManager<IdentityUser>>();
            await EnsureTestAdminAsync(userManager);
        }

        private static async Task EnsureTestAdminAsync(UserManager<IdentityUser> userManager)
        {
            var testAdmin = await userManager.Users
                .Where(x => x.Email == "admin@haiengi.com")
                .SingleOrDefaultAsync();

            if (testAdmin != null) return;

            testAdmin = new Admin
            {
                UserName = "admin",
                Email = "admin@haiengi.com"
            };
            await userManager.CreateAsync(
                testAdmin, "Adm!n1");
            await userManager.AddToRoleAsync(
                testAdmin, Constants.AdminRole);

            var testAdmin2 = await userManager.Users
                .Where(x => x.Email == "admin2@haiengi.com")
                .SingleOrDefaultAsync();

            if (testAdmin2 != null) return;

            testAdmin = new Admin
            {
                UserName = "admin2",
                Email = "admin2@haiengi.com"
            };
            await userManager.CreateAsync(
                testAdmin, "Adm!n2");
            await userManager.AddToRoleAsync(
                testAdmin, Constants.AdminRole);
        }

        private static async Task EnsureRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            var alreadyExists = await roleManager
                .RoleExistsAsync(Constants.AdminRole);

            if (alreadyExists) return;

            await roleManager.CreateAsync(
                new IdentityRole(Constants.AdminRole));
        }
    }
}
