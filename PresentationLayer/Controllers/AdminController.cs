﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogicLayer.AdminBusiness;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;

namespace PresentationLayer.Controllers
{
    [Authorize(Roles = Constants.AdminRole)]
    public class AdminController : Controller
    {
        private readonly IAdminService adminService;
        private readonly IMapper mapper;

        public AdminController(IAdminService adminService, IMapper mapper)
        {
            this.adminService = adminService;
            this.mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ViewAllUsers()
        {
            IEnumerable<
                IdentityUser> users = await adminService.GetAllUsers();

            List<UserForOverviewDTO> userForOverviewDtos = mapper.Map<List<UserForOverviewDTO>>(users);

            return View(userForOverviewDtos);
        }

        [HttpGet]
        public async Task<IActionResult> DetailedRegularUsers(string id)
        {
            IEnumerable<RegularUser> regularUsers = await adminService.GetRegularUsers();
            RegularUser user = regularUsers.FirstOrDefault(x => x.Id.Equals(id));

            RegularUserForDetailsDTO userForDetailsDto = mapper.Map<RegularUserForDetailsDTO>(user);

            return View(userForDetailsDto);
        }

        public IActionResult AddCompany()
        {
            ViewBag.Currencies = adminService.GetAllCurrencies().Result.ToList();

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddCompany(Company company)
        {
            if (company.Name == null || company.Name.Equals(String.Empty))
            {
                TempData["ErrorMessage"] = "Company name must have a value";
                return RedirectToAction(nameof(AddCompany));
            }

            company.Id = Guid.NewGuid();

            try
            {
                adminService.AddCompany(company);
                return RedirectToAction(nameof(ViewAllCompanies));
            }
            catch (ValidationException e)
            {
                TempData["ErrorMessage"] = e.Message;
            }

            return View(company);
        }

        public async Task<IActionResult> ViewAllCompanies()
        {
            IEnumerable<Company> companies = await adminService.GetAllCompanies();

            return View(companies);
        }

        public async Task<IActionResult> AssignRandomBills(string id)
        {
            IEnumerable<IdentityUser> users = adminService.GetAllUsers().Result;

            RegularUser user = users.FirstOrDefault(x => x.Id.Equals(id)) as RegularUser;

            adminService.GenerateRandomBills(user);

            return RedirectToAction(nameof(DetailedRegularUsers), new { id });
        }

        public async Task<IActionResult> NotifyViaEmailOfNegativeAccountAmount(string userId, int accountId)
        {
            RegularUser user = adminService.GetRegularUsers().Result.FirstOrDefault(u => u.Id.Equals(userId));

            Account account = user?.Accounts.FirstOrDefault(a => a.Id == accountId);

            if (user != null && account != null)
            {
                try
                {
                    adminService.NotifyUserOfNegativeAccountSum(user, account);
                }
                catch (ValidationException e)
                {
                    TempData["ErrorMessage"] = e.Message;
                }
            }
            else
            {
                TempData["ErrorMessage"] = "User and account could not be found";
            }

            return RedirectToAction(nameof(Index));
        }
    }
}