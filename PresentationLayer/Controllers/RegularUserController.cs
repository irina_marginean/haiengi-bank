﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogicLayer.RegularUserBusiness;
using BusinessLogicLayer.Transactions;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;

namespace PresentationLayer.Controllers
{
    [Authorize(Roles = Constants.RegularUserRole)]
    public class RegularUserController : Controller
    {
        private readonly IRegularUserService regularUserService;
        private readonly UserManager<IdentityUser> userManager;
        private readonly IMapper mapper;
        private readonly IEmailSender emailSender;

        public RegularUserController(IRegularUserService regularUserService, 
            UserManager<IdentityUser> userManager, IMapper mapper, IEmailSender emailSender)
        {
            this.regularUserService = regularUserService;
            this.userManager = userManager;
            this.mapper = mapper;
            this.emailSender = emailSender;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Details()
        {
            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            RegularUser currentRegularUser = regularUserService.GetRegularUser(currentUser.Id);

            RegularUserForDetailsDTO regularUserForDetailsDto =
                mapper.Map<RegularUserForDetailsDTO>(currentRegularUser);   

            return View(regularUserForDetailsDto);
        }

        public IActionResult CreateAccount()
        {
            ViewBag.Currencies = regularUserService.GetAllCurrencies().Result;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAccount(AccountForCreationDTO accountForCreationDto)
        {
            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            RegularUser currentRegularUser = regularUserService.GetRegularUser(currentUser.Id);

            try
            {
                regularUserService.CreateAccount
                    (currentRegularUser, accountForCreationDto.Type, accountForCreationDto.Currency);

                TempData["SuccessMessage"] = "Successfully added a new account!";

                return RedirectToAction(nameof(Details));
            }
            catch (ValidationException e)
            {
                TempData["ErrorMessage"] = e.Message;
            }

            return View(accountForCreationDto);
        }

        [ActionName(nameof(AccountDetails))]
        [HttpGet]
        public async Task<IActionResult> AccountDetails(string id)
        {
            int accountId = Int32.Parse(id);
            AccountForDetailsDTO accountForDetailsDto;

            SpendingAccount spendingAccount = regularUserService.GetSpendingAccount(accountId);

            if (spendingAccount != null)
            {
                accountForDetailsDto = mapper.Map<AccountForDetailsDTO>(spendingAccount);
            }
            else
            {
                SavingAccount savingAccount = regularUserService.GetSavingAccount(accountId);

                accountForDetailsDto = savingAccount != null ? mapper.Map<AccountForDetailsDTO>(savingAccount) : new AccountForDetailsDTO();
            }

            return View(accountForDetailsDto);
        }

        public IActionResult DepositSaving(string id)
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> DepositSaving(string id,
            [Bind("Amount, NumberOfMonths")] DepositViewModel deposit)
        {
            int accountId = Int32.Parse(id);

            try
            {
                SavingAccount savingAccount = regularUserService.GetSavingAccount(accountId);

                await regularUserService.DepositMoneyInSavingAccount(savingAccount, deposit.Amount, deposit.NumberOfMonths);

                return RedirectToAction(nameof(Details));
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message;
            }

            return View(deposit);
        }

        public IActionResult DepositSpending(string id)
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> DepositSpending(string id,
            [Bind("Amount")] DepositViewModel deposit)
        {
            int accountId = Int32.Parse(id);

            try
            {
                SpendingAccount spendingAccount = regularUserService.GetSpendingAccount(accountId);

                await regularUserService.DepositMoneyInSpendingAccount(spendingAccount, deposit.Amount);

                return RedirectToAction(nameof(Details));
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message;
            }

            return View(deposit);
        }

        public async Task<IActionResult> PayBill()
        {
            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            RegularUser currentRegularUser = regularUserService.GetRegularUser(currentUser.Id);

            IEnumerable<Account> accounts = regularUserService.GetAccounts(currentRegularUser).Result;

            ViewBag.Accounts = accounts;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> PayBill(string id, 
            AccountForPaymentDTO accountForPaymentDTO)
        {
            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            RegularUser currentRegularUser = regularUserService.GetRegularUser(currentUser.Id);

            Bill bill = regularUserService.GetBill(id);

            Account account = regularUserService.GetAccount(accountForPaymentDTO.Account);

            try
            {
                regularUserService.PayBill(currentRegularUser, bill, account);

                await emailSender.SendEmailAsync(
                                    currentRegularUser.Email,
                                    "Bill payment",
                                    "Thank you for your payment.\n" +
                                    $"Bill paid for company {bill.Company.Name}, in {bill.Company.Currency}, having " +
                                    $"the amount {bill.Amount}, on date {bill.DatePaid.Date}.\n " +
                                    $"The account you used has the IBAN {account.IBAN} andthe currency {account.Currency}, " +
                                    $"and is of {account.Type} type.\n " +
                                    "Have a nice day, \nThe HaiEnGi Bank");

                return RedirectToAction(nameof(Details));
            }
            catch(ValidationException e)
            {
                TempData["ErrorMessage"] = e.Message;
            }

            return RedirectToAction(nameof(Details));
        }

        public async Task<IActionResult> DisplayTransactions(string dateFrom, string dateTo)
        {
            DateViewModel dateViewModel = null;

            if (dateFrom != null && dateFrom != String.Empty &&
                dateTo != null && dateTo != String.Empty)
            {
                IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

                RegularUser currentRegularUser = regularUserService.GetRegularUser(currentUser.Id);

                try
                {             
                    List<Transaction> transactions = regularUserService
                        .GetTransactions(currentRegularUser, Convert.ToDateTime(dateFrom), Convert.ToDateTime(dateTo));

                    if (transactions == null || transactions.Count == 0)
                    {
                        TempData["ErrorMessage"] = "No transactions to display for this period range.";
                    }
                    else
                    {
                        List<TransactionForExportDTO> transactionForExportDTOs = new List<TransactionForExportDTO>();
                    
                        foreach (var transaction in transactions)
                        {
                            transactionForExportDTOs.Add(mapper.Map<TransactionForExportDTO>(transaction));
                        }

                        dateViewModel = new DateViewModel
                        {
                            Transactions = transactionForExportDTOs
                        };
                    }
                }
                catch(ValidationException e)
                {
                    TempData["ErrorMessage"] = e.Message;
                }
            }
           
           return View(dateViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> DownloadTransactions(string type, List<string> transactionNames)
        {
            if (transactionNames == null || transactionNames.Count == 0)
            {
                TempData["ErrorMessage"] = "No transaction to export to file was selected!";

                return RedirectToAction(nameof(DisplayTransactions));
            }

            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            RegularUser currentRegularUser = regularUserService.GetRegularUser(currentUser.Id);

            List<Transaction> transactions = new List<Transaction>();
               
            foreach (var transactionName in transactionNames)
            {
                Transaction transaction =  regularUserService
                .GetTransactions(currentRegularUser)
                .Find(t => t.Id.ToString().Equals(transactionName));

                transactions.Add(transaction);
            }

            ExportStrategy exportStrategy = null;

            if (type.Equals("pdf"))
            {
                exportStrategy = new ExportStrategy(new PDFExportStrategy());
            }
            if (type.Equals("csv"))
            {
                exportStrategy = new ExportStrategy(new CSVExportStrategy());
            }

            return File(exportStrategy.GetExportBytes(transactions), exportStrategy.GetFileType(), exportStrategy.GetFileName());
        }
    }
}