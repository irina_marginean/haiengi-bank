﻿namespace PresentationLayer
{
    public static class Constants
    {
        public const string AdminRole = "Admin";
        public const string RegularUserRole = "RegularUser";
    }
}
