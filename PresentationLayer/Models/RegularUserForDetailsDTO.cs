﻿using System;
using System.Collections.Generic;
using DataAccessLayer.Models;

namespace PresentationLayer.Models
{
    public class RegularUserForDetailsDTO
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateCreated { get; set; }
        public List<Account> Accounts { get; set; }
        public List<Bill> Bills { get; set; }
        
    }
}
