﻿using System;

namespace PresentationLayer.Models
{
    public class AccountForDetailsDTO
    {
        public string IBAN{ get; set; }
        public  DateTime DateCreated { get; set; }
        public  DateTime DateModified { get; set; }
        public string Currency { get; set; }
        public string Type { get; set; }
        public double Sum { get; set; }
        public double Percentage { get; set; }
    }
}
