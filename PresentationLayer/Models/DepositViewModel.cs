﻿using System.ComponentModel.DataAnnotations;
using DataAccessLayer.Models;

namespace PresentationLayer.Models
{
    public class DepositViewModel
    {
        [Required]
        [Range(0.1, 10000000.0)]
        public double Amount { get; set; }
        public int NumberOfMonths { get; set; }
    }
}
