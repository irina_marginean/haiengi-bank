﻿using DataAccessLayer.Models;
using System;

namespace PresentationLayer.Models
{
    public class TransactionForExportDTO
    {
        public Guid Id {get; set;}
        public string Type {get; set;}
        public double Amount {get; set;}
        public RegularUser User {get; set;}
        public Account Account {get; set;}
        public Bill Bill {get; set;}
        public DateTime Date {get; set;}
        public bool IsChecked { get; set; }
    }
}
