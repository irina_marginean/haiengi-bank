﻿using DataAccessLayer.Models;
using System.Collections.Generic;

namespace PresentationLayer.Models
{
    public class DateViewModel
    {      
        public List<TransactionForExportDTO> Transactions { get; set; }
        public string Type { get; set; }

    }
}
