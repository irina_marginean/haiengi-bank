﻿namespace PresentationLayer.Models
{
    public class AccountForCreationDTO
    {
        public string Currency { get; set; }
        public string Type { get; set; }
    }
}
