﻿namespace PresentationLayer.Models
{
    public class UserForOverviewDTO
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
    }
}
