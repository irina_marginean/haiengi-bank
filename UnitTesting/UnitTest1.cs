using BusinessLogicLayer.Helpers;
using BusinessLogicLayer.Utils;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace UnitTesting
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestRandomNumberGenerator()
        {
            int number = NumberGenerator.GetRandomNumberInRange(1, 9);

            Assert.IsTrue(number >= 1 && number <= 9);
        }

        [TestMethod]
        public void TestBCrypt()
        {
            IPasswordHasher<IdentityUser> passwordHasher = new BCryptPasswordHasher();

            IdentityUser regularUser = new RegularUser();
            string password = "my_pass";
            string hashedPassword = passwordHasher.HashPassword(regularUser, password);

            Assert.IsTrue(passwordHasher.VerifyHashedPassword(regularUser, hashedPassword, password) == 
                PasswordVerificationResult.Success);
        }

        [TestMethod]
        public void TestConversionRate()
        {
            double rate = CurrencyConverter.GetExchangeRate("eur", "ron");
            
            Assert.IsTrue(rate > 400 && rate < 500);
        }
    }
}
